#include <stdio.h>
#include <stdlib.h>

#include "visibility.h"

// visibility attributes not working like I imagine the should. still able to call both in main.c

void __attribute ((visibility("internal"))) func1(int i)
{
    printf("Func1: %d\n", i);
}


void __attribute ((visibility("default"))) func2(int i)
{
    //void (*fun_ptr)(int) = &func1;
    //(*fun_ptr)(i);
    printf("func2: %d\n", i);
}
