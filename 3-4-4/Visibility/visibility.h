#ifndef VISIBILIY_H
#define VISIBILIY_H

void __attribute ((visibility("internal"))) func1(int i);

void __attribute ((visibility("default"))) func2(int i);

#endif