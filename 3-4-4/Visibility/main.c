#include <stdio.h>
#include <stdlib.h>

#include "visibility.h"


int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        printf("Ussage: %s <INT>", argv[0]);
        exit(1);
    }

    int num = strtol(argv[1], NULL, 10);

    func2(num);
    func1(num);
}