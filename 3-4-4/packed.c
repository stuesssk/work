#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


struct original
{//2 + 18 + 4 + 32 = 56
    uint16_t num1;
    char string1[17];  //alocated 18
    uint32_t num2;
    char string2[29];  // allocated 32
};

struct __attribute__((packed)) compressed
{ //2 + 17 + 4 + 29 = 52
    uint16_t num1;
    char string1[17];
    uint32_t num2;
    char string2[29];
};

void check_sizes()
{
    printf("Size of packed struct: %ld\n", sizeof(struct compressed));
    struct compressed *smaller = malloc(sizeof(struct compressed));

    printf("Size of normal struct: %ld\n", sizeof(struct original));
    struct original *larger = malloc(sizeof(struct original));

    printf("Unpacked Addresses");
    printf("larger.num1     :%p\n", &larger->num1);
    printf("larger.string1  :%p\n", &larger->string1);
    printf("larger.num2     :%p\n", &larger->num2);
    printf("larger.string1  :%p\n", &larger->string2);

    printf("Packed Addresses\n");
    //printf("smaller.num1:    %p\n", &smaller->num1);
    printf("smaller.string1: %p\n", &smaller->string1);
    //printf("smaller.num1:    %p\n", &smaller->num2);
    printf("smaller.string2: %p\n", &smaller->string2);

    printf("Size of unpacked items\n");
    printf("size string1: %ld\n", sizeof(larger->string1));
    printf("size string2: %ld\n", sizeof(larger->string2));
    printf("size num1: %ld\n", sizeof(larger->num1));
    printf("size num2: %ld\n", sizeof(larger->num2));

    printf("Size of packed items\n");
    printf("size string1: %ld\n", sizeof(smaller->string1));
    printf("size string2: %ld\n", sizeof(smaller->string2));
    printf("size num1: %ld\n", sizeof(smaller->num1));
    printf("size num2: %ld\n", sizeof(smaller->num2));
    //packed only effects how the struct is alligned in memory. does not change the type sizes
}

int main(void)
{
    check_sizes();
    return 0;
}