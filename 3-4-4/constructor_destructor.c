#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * globalvar;

//constructors are called before main, in order of least priority to largest number
void setup0(void) __attribute__((constructor (101)));
void setup1(void) __attribute__((constructor (110)));
//destructors are called after main completes or exit is called and are called largest to smallest priority
void cleanup0(void)  __attribute__((destructor (110)));
void cleanup1(void)  __attribute__((destructor (101)));

void setup0(void)
{
    printf("Running first before main: %s\n", __func__);
}

void setup1(void)
{
    printf("Running secound before main: %s\n", __func__);
    char * teststring = "I made this before main runs";
    globalvar = calloc(1, strlen(teststring));
    snprintf(globalvar, strlen(teststring), "%s", teststring);
}
void cleanup_str(char ** ptr)
{
    //cleanup attribute happens before destructor
    printf("Runnig When variable falls out of scope: %s\n", __func__);
    printf("%s\n", *ptr);
}

void cleanup0(void)
{
    printf("Runing first cleanup function after main: %s\n", __func__);
    printf("Freeing premain string: %s\n", globalvar);
    free(globalvar);
}

void cleanup1(void)
{
    printf("Running secound cleanup function after main: %s\n", __func__);
}

int main(void)
{
    printf("Start of main\n");

    char * test __attribute__ ((cleanup(cleanup_str))) = calloc(1,32);
    test = "I made this in main";

    return 0;

}