# include <stdlib.h>
# include <stdio.h>

# define TMP_FILE "/tmp/tmp.file"

struct mystruct
{
    char *string1;
    char *string2;
    int num1;
};

void cleanup_file(FILE **file)
{
    printf("Closing file\n");
    fclose(*file);

    printf("Deleting the file\n");
    remove(TMP_FILE);
}

void free_buffer(char **buff)
{
    printf("freeing buffer\n");
    free(*buff);
}

void cleanup_struct(struct mystruct **sp)
{
    printf("cleaning up the Struct\n");

    printf("freeing string1\n");
    free((*sp)->string1);
    printf("freeing string2\n");
    free((*sp)->string2);
    printf("freeing the struct\n");
    free(*sp);

}

int main(void)
{
    /*
        This side tracked me after I started reading on the __cleanup__ attribute

        __cleanup__(fucntion) attribute calls the associate function once the variable 
        falls out of scope (i.e. when returning from a fucntion), allows you to not forget
        about closing a fiel of freeing memory.

        Real cool attribute in my opinion.
    */
    char *buffer __attribute__ ((__cleanup__(free_buffer))) = malloc(64);
    FILE *fp __attribute__ ((__cleanup__(cleanup_file)));

    fp = fopen(TMP_FILE, "w+");

    if (fp != NULL)
    {
        fprintf(fp, "%s", "ThisIsMyTestString,ItIsLong");
    }

    fflush(fp);
    fseek(fp, 0L, SEEK_SET);
    fscanf(fp, "%s", buffer);
    printf("%s\n", buffer);

    struct mystruct *test __attribute__ ((__cleanup__(cleanup_struct))) = calloc(1, sizeof(struct mystruct));
    test->string1 = calloc(1,12);
    test->string2 = calloc(1,64);

    return 0;
}