#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>


#define CACHE_TYPE 0x1F
#define CACHE_LVL  0xE0
#define INIT_CACHE 0x100
#define ASSOC_CACHE 0x200
#define NUM_THREADS 0x3FFC000
#define NUM_PROCS 0xFC000000

#define BRAND_INDEX 0xFF
#define CLFLUSH_SZ 0xFF00
#define MAX_ADDRESS_IDS 0xFF0000
#define APIC_ID 0xFF000000

#define EX_FAM_ID 0xFF00000
#define EX_MODEL_ID 0xF0000
#define PROC_TYPE 0x3000
#define FAMILY_ID 0xF00
#define MODEL 0xF0
#define STEPPING_ID 0xF

#define BRAND_STRING 0x80000000
#define MAX_EXT_CPUID 0x80000004


typedef struct cpuinfo
{
    char *   vendor;
    char *   modelName;
    uint32_t base_freq;
    uint32_t max_freq;
    uint32_t ref_freq;
    uint32_t cpuid;
    uint8_t  extended_fam_id;
    uint8_t  extended_model_id;
    uint8_t  processor_type;
    uint8_t  family_id;
    uint8_t  model;
    uint8_t  stepping_id;
    uint8_t  brand_index;
    uint8_t cl_flush_sz;
    uint8_t max_addresssable_ids;
    uint8_t  apic_id;
    uint8_t  cache_type;
    uint8_t  cache_lvl;
    uint8_t  init_cache;
    uint8_t  associative_cache;
    uint8_t  num_threads_shared;
    uint8_t  num_procs;

}cpuinfo_t;

static void cpu(uint32_t input1, uint32_t input2,
                uint32_t * output1, uint32_t * output2,
                uint32_t * output3, uint32_t * output4 )
{
    __asm__("cpuid" //command
            :"=a" (*output1), "=b" (*output2), "=c" (*output3), "=d" (*output4)      //outputs
            :"a" (input1), "b" (input2) //inputs
            :);    //clobbers

}

void printcpu(cpuinfo_t *info)
{
    printf("Processor: %d\n", info->processor_type);
    printf("Vendor ID: %s\n", info->vendor);
    printf("CPU Family ID: %d\n", info->cpuid);
    printf("Model: %d\n", info->model);
    printf("Model Name: %s\n", info->modelName);
    printf("Stepping ID: %d\n", info->stepping_id);
    printf("CPU ID: %d\n", info->cpuid);
    printf("Extended Family Id: %d\n", info->extended_fam_id);
    printf("Extended Model Id: %d\n", info->extended_model_id);
    printf("CL Flush Size: %d\n", info->cl_flush_sz);
    printf("APIC IDL: %d\n", info->apic_id);
    printf("Addresses: %d\n", info->max_addresssable_ids);
}

static void strconvert(char *string, uint32_t num)
{
    for (int i = 0; i < sizeof(num); i++)
    {
        string[i] = num >> (i * 8);
    }
}

static bool brandStingSupport()
{
    uint32_t eax = 0;
    uint32_t ebx = 0;
    uint32_t ecx = 0;
    uint32_t edx = 0;
    
    cpu(BRAND_STRING, 0, &eax, &ebx, &ecx, &edx);

    if(eax & BRAND_STRING)
    {
        if( eax == MAX_EXT_CPUID)
            return true;
    } 
    return false;
}

void modelName(cpuinfo_t *info, int value)
{

    // test if cpu supports getting the processor brand sting
    if(!brandStingSupport())
    {
        printf("Brand String Not Supported\n");
        return;
    }


    uint32_t eax = 0;
    uint32_t ebx = 0;
    uint32_t ecx = 0;
    uint32_t edx = 0;
    info->modelName = calloc(64, 1);

    for(int i = 0; i < 3; ++i)
    {
        int offset = i * 16;
        cpu(0x80000002 + i,0,&eax,&ebx,&ecx,&edx);
        strconvert(info->modelName +offset, eax);
        strconvert(info->modelName +4 +offset, ebx);
        strconvert(info->modelName +8 +offset, ecx);
        strconvert(info->modelName +12 +offset, edx);
    }
}



void cpuId(cpuinfo_t *info)
{
    uint32_t eax = 0;
    uint32_t ebx = 0;
    uint32_t ecx = 0;
    uint32_t edx = 0;

    cpu(0,0,&eax,&ebx,&ecx,&edx);
    info->cpuid = eax;
    info->vendor = calloc(12, 1);
    strconvert(info->vendor, ebx);
    strconvert(info->vendor +4, edx);
    strconvert(info->vendor +8, ecx);
}

void cpuFrequencies(cpuinfo_t *info)
{
    // skylake/kaybylake and newer processors only
    uint32_t eax = 0;
    uint32_t ebx = 0;
    uint32_t ecx = 0;
    uint32_t edx = 0;

    cpu(0x16,0,&eax,&ebx,&ecx,&edx);
    info->base_freq  = eax;
    info->max_freq = ebx;
    info->ref_freq = ecx;
    //printf("Base: %d\nMax: %d\nRef: %d\n", eax,ebx,ecx);
}

void procFeatures(cpuinfo_t *info)
{
    uint32_t eax = 0;
    uint32_t ebx = 0;
    uint32_t ecx = 0;
    uint32_t edx = 0;

    cpu(1,0,&eax,&ebx,&ecx,&edx);

    info->extended_fam_id = (eax & EX_FAM_ID) >> 20;
    info->extended_model_id = (eax & EX_MODEL_ID) >>16;
    info->processor_type = (eax & PROC_TYPE) >> 12;
    info->family_id = (eax & FAMILY_ID) >>8;
    info->model = (eax & MODEL) >> 4;
    info->stepping_id = eax & STEPPING_ID;

    info->brand_index = ebx & BRAND_INDEX;
    info->cl_flush_sz = ((ebx & CLFLUSH_SZ) >> 8) * 8;
    info->max_addresssable_ids = (ebx & MAX_ADDRESS_IDS) >> 16;
    info->apic_id = (ebx & APIC_ID) >> 28;
}

void threadCoreInfo(cpuinfo_t *info)
{
    uint32_t eax = 0;
    uint32_t ebx = 0;
    uint32_t ecx = 0;
    uint32_t edx = 0;

    cpu(1,0,&eax,&ebx,&ecx,&edx);

    info->cache_type = eax & CACHE_TYPE;
    info->cache_lvl = (eax & CACHE_LVL) >> 4;
    info->init_cache = (eax & INIT_CACHE) >> 8;
    info->associative_cache = (eax & ASSOC_CACHE) >> 8 ;
    info->num_threads_shared = (eax &  NUM_THREADS) >> 12;
    info->num_procs = (eax & NUM_PROCS) >> 24;
}

int main(void)
{
    cpuinfo_t info = {0};
    cpuId(&info);
    procFeatures(&info);
    modelName(&info,1);
    modelName(&info,2);
    modelName(&info,3);
    printf("\n");
    //printf("CPUID: %x\n", info.cpuid);
    //printf("Vendor: %s\n", info.vendor);
    printcpu(&info);
    cpuFrequencies(&info);

}