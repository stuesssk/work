#ifndef TCPSYN_H
#define TCPSYN_H

#include <arpa/inet.h>
#include <ctype.h>
#include <ifaddrs.h>
#include <ifaddrs.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

struct __attribute__((packed)) ipheader {
    uint8_t ip_hl:4;  // :4 will only allocate 4 bits
    uint8_t ip_v:4; 
    uint8_t ip_tos; // dscp 6 bit + ecn 2 bits
    uint16_t ip_len;
    uint16_t ip_id;
    uint16_t ip_off; //flags 3 bits + offset 13 bits
    uint8_t ip_ttl;
    uint8_t ip_p;
    uint16_t ip_sum;
    uint32_t ip_src;
    uint32_t ip_dst;
};

struct __attribute__((packed)) tcpheader {
    uint16_t th_sport;
    uint16_t th_dport;
    uint32_t th_seq;
    uint32_t th_ack;
    uint8_t th_resv:4; //reserved 3 bits + NS 1 bit
    uint8_t th_off:4;
    uint8_t th_flags;
    uint16_t th_win;
    uint16_t th_sum;
    uint16_t th_urp;
};

struct threadinfo
{
    uint16_t start_port;
    uint16_t end_port;
    uint32_t dest_ip;
    uint32_t timeout;
};

//pseudo header for calculating tcp checksum
struct __attribute__((packed)) pseudoheader
{
    uint32_t sIP;
    uint32_t dIP;
    uint8_t reserved;
    uint8_t protocol;
    uint16_t tcpLen;
    struct tcpheader tcp;
};

//TCP flag values
#define	TH_FIN	0x01
#define	TH_SYN	0x02
#define	TH_RST	0x04
#define	TH_PUSH	0x08
#define	TH_ACK	0x10
#define	TH_URG	0x20

#define ETHERNET_HDR 18
#define SRVR_PORT    54321
#define BUFF_SZ      2048
#define TIMEOUT      30

#define SIG_WORKING 1
#define SIG_DONE    2


static uint16_t checksum(unsigned short* ptr, int nbytes);
uint32_t generateNum(void);
void gethostIP(char *ipaddress);
void pophdrs(void *buff, uint16_t port, uint32_t destIP);
void *sendFunc(void *vargp);
uint16_t process_packets(void * buffer, ssize_t len);
void *recvFunc(void *vargp);
int strtoport(char *str);

int signaler;
pthread_mutex_t lock; 

#endif