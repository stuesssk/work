#include "tcpsyn.h"


int main (int argc, char *argv[])
{
    if(argc !=4){
        printf("Usage: %s <Dest IP> <Start Port> <End Port>\n", argv[1]);
        exit(1);
    }

    int ret;
    // Test to ensure recieving valid IP Address
    struct sockaddr_in sa;
    ret = inet_pton(AF_INET, argv[1], &(sa.sin_addr));
    if(ret == 0)
    {
        printf("Usage: %s <Dest IP> <Start Port> <End Port>\n", argv[1]);
        exit(2);
    }

    // capture start port
    int sport = strtoport(argv[2]);
    if(sport == 0)
    {
        fprintf(stderr, "error: invlaid port number, port: %d\n", argv[2]);
        exit(3);
    }

    // capture end port
    int eport = strtoport(argv[3]);
    if(eport == 0)
    {
        fprintf(stderr, "error: invlaid port number, port: %d\n", argv[5]);

        exit(4);
    }

    // Seed RNG
    time_t t;
    srand((unsigned) time(&t));

    
    pthread_t threadSend, threadRecv;
    int rc;
    struct threadinfo *info= calloc(1, sizeof(struct threadinfo));
    info->start_port = sport;
    info->end_port = eport;
    info->dest_ip = inet_addr(argv[1]);
    // Scan timeout of 3s per port scanned 
    info->timeout = (eport - sport + 1) * 3;

    //signaler = SIG_WORKING;

    if (pthread_mutex_init(&lock, NULL) != 0) { 
        printf("\n mutex init has failed\n"); 
        return 1; 
    } 
    pthread_mutex_lock(&lock);

    /*struct threadinfo *inforecv= calloc(1, sizeof(struct threadinfo));
    inforecv->start_port = sport;
    inforecv->end_port = eport;
    inforecv->dest_ip = inet_addr(argv[1]);*/

    printf("Scan Report for %s:\n", argv[1]);

    if ((rc = pthread_create(&threadSend, NULL, sendFunc, (void *)info)))
    {
        fprintf(stderr, "error: pthread_create, rc: %d\n", rc);
        return EXIT_FAILURE;
    }
    if ((rc = pthread_create(&threadRecv, NULL, recvFunc, (void *)info)))
    {
        fprintf(stderr, "error: pthread_create, rc: %d\n", rc);
        pthread_join(threadSend, NULL);
        return EXIT_FAILURE;
    }


    
    pthread_join(threadSend, NULL);
    pthread_join(threadRecv, NULL);
    printf("Threads Joined Exiting\n");
    free(info);
    //free(inforecv);
    return EXIT_SUCCESS;
}

static uint16_t checksum(unsigned short* ptr, int nbytes)
{
    register long sum;
    register short answer;
    unsigned short oddbyte;

    sum = 0;
    while (nbytes > 1) {
        sum += *ptr++;
        nbytes -= 2;
    }

    if (nbytes == 1) {
        oddbyte = 0;
        *((uint8_t*)&oddbyte) = *(uint8_t*)ptr;
        sum += oddbyte;
    }

    sum = (sum >> 16) + (sum & 0xffff);
    sum = sum + (sum >> 16);
    answer = (short)~sum;

    return answer;
}

uint32_t generateNum(void)
{
    uint32_t val;
    val = rand();
    return val;
}



void gethostIP(char *ipaddress)
{
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;

    getifaddrs(&ifAddrStruct);

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
            // is a valid IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            // Add check to ignore if lo interface
            char *ret = strstr(ifa->ifa_name, "lo");
            if(ret == NULL)
            {
            memcpy(ipaddress, addressBuffer, strlen(addressBuffer));
            }
        }
    }
    if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);
}

void pophdrs(void *buff, uint16_t port, uint32_t destIP)
{
    //point ip header to beginging of buffer
    struct ipheader *iph = (struct ipheader *) buff;
    // Point tcp header to buffer offset by the size of the ip header
    struct tcpheader *tcph = (struct tcpheader *) (buff + sizeof (struct ipheader));
    iph->ip_hl = 5;
    iph->ip_v = 4;
    iph->ip_tos = 0;
    iph->ip_len = sizeof(struct ipheader) + sizeof(struct tcpheader);
    iph->ip_id = htons(54321); //number doens't really mater
    iph->ip_off = 0;
    iph->ip_ttl = 255;
    iph->ip_p = IPPROTO_TCP;
    iph->ip_sum = 0; // set to 0 before calculating
    // obtain host IP address
    char *sIP = calloc(1, INET_ADDRSTRLEN);
    gethostIP(sIP);
    iph->ip_src = inet_addr(sIP);
    free(sIP);
    iph->ip_dst = destIP;

    tcph->th_sport = htons(SRVR_PORT);
    tcph->th_dport = htons(port);
    size_t val = generateNum();
    tcph->th_seq = htonl(12345);
    tcph->th_ack = 0;
    tcph->th_resv = 0;
    tcph->th_off = sizeof(struct tcpheader) / 4; // size of tcp header
    tcph->th_flags = TH_SYN;
    tcph->th_win = htons(14600); // max allowable size
    tcph->th_sum = 0; // Kernal's IP stack should fill this in
    tcph->th_urp = 0;

    // Wireshark shows IP Check sum is correct
    iph->ip_sum = checksum((unsigned short*)buff, iph->ip_len >> 1);

    char *tcpcheck = calloc(1, (sizeof(struct pseudoheader)));
    struct pseudoheader * temp = (struct pseudoheader *)tcpcheck;
    temp->dIP = iph->ip_dst;
    temp->sIP = iph->ip_src;
    temp->protocol = IPPROTO_TCP;
    temp->reserved = 0;
    temp->tcpLen = htons(sizeof(struct tcpheader));
    temp->tcp = *tcph;

    // Wireshark shows proper checksum and that this was the the cause of packets being dropped
    int ret = checksum((unsigned short *)tcpcheck, (sizeof(struct pseudoheader)));
    tcph->th_sum = ret; 
}

void *sendFunc(void *vargp)
{
    struct threadinfo *info = (struct threadinfo *)vargp;
    
    //Open rawsocket
    int s = socket(PF_INET, SOCK_RAW, IPPROTO_TCP);
    if(s == -1)
    {
        perror("socket() error");
        exit(2);
    }
    // buffer for ip and tcp headers
    char *datagram = calloc(1, 4096);
    //point ip header to beginging of buffer
    struct ipheader *iph = (struct ipheader *) datagram;
    // Point tcp header to buffer offset by the size of the ip header
    struct tcpheader *tcph = (struct tcpheader *) datagram + sizeof (struct ipheader);

    struct sockaddr_in sin;
			/* the sockaddr_in containing the dest. address is used
			   in sendto() to determine the datagrams path */

    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = info->dest_ip;
    
    
    //populate headers
    for(int i = info->start_port; i<= info->end_port; ++i){
        //populate headers
        pophdrs(datagram, i, info->dest_ip);
        sin.sin_port = htons (i);

        /*
            it is very advisable to do a IP_HDRINCL call, to make sure
            that the kernel knows the header is included in the data, and doesn't
            insert its own header into the packet before our data
        */
        int one = 1;
        const int *val = &one;
        if(setsockopt(s, IPPROTO_IP, IP_HDRINCL, val, sizeof(one)) < 0)
        {
            perror("setsockopt() error");
            exit(2);
        }
        // send syn
        if (sendto (s,datagram,iph->ip_len,0,(struct sockaddr *) &sin,sizeof (sin)) < 0)
	    {
            printf ("error\n");
        }
//For testing  to ensure packet got written correctly
#if 0
            for(int j = 0; j <= iph->ip_len; ++j)
            {
                printf("Byte[%d]: %x\n", j, datagram[j]);
            }
#endif

    }
    free(datagram);
    //signaler = SIG_DONE;
    pthread_mutex_unlock(&lock);
    pthread_exit(NULL);
}

uint16_t process_packets(void * buffer, ssize_t len)
{
    //printf("processing\n");
    //printf("RECIEVE PACKET Data: %d\n", len);
    struct ipheader* iph = (struct ipheader*)buffer;
    uint16_t iphdrlen = iph->ip_hl * 4;

    struct tcpheader* tcph = (struct tcpheader*)(buffer + iphdrlen);

    if (iph->ip_p == IPPROTO_TCP) 
    {
        //printf("In \n");


        if (tcph->th_flags == (TH_SYN | TH_ACK)) {

            printf("Port %d open\n", ntohs(tcph->th_sport));
            fflush(stdout);
        }
    }
    //printf("IP len: %d\n", iphdrlen);
/*
    char *test = (char *)buffer;
    for(int i = 0; i <=52; ++i)
    {
        printf("%x|", test[i]);
    }*/

    
    return ntohs(tcph->th_sport);
}

void *recvFunc(void *vargp)
{
    time_t start = time(NULL);
    time_t now = time(NULL);
    struct threadinfo *recv = (struct threadinfo *)vargp;
    // Not the same in sending and recieving
    // struct getting messed with somewhere

    int sock_raw;

    socklen_t saddr_size, data_size;
    struct sockaddr_in saddr;

    unsigned char* buffer = (unsigned char*)malloc(BUFF_SZ);

    //Create a raw socket that will sniff
    sock_raw = socket(AF_INET, SOCK_RAW, IPPROTO_TCP);
    if (sock_raw < 0) {
        printf("Socket Error\n");
        fflush(stdout);
        free(buffer);
        pthread_exit(NULL);
    }

    saddr_size = sizeof(saddr);

 
    //Receive a packet

    // Need to filter out all data not from target
    uint16_t retval = 0;
    while(1)
    {
        data_size = recvfrom(sock_raw, buffer, BUFF_SZ, 0, (struct sockaddr*)&saddr, &saddr_size);
        if (data_size < 0) {
            printf("Recvfrom error, failed to get packets\n");
            fflush(stdout);
        }

        if(saddr.sin_addr.s_addr == recv->dest_ip)
        {
            retval = process_packets(buffer, data_size);
            fflush(stdout);
        }
        // still working on way to signal from send thread it is done and this thread can time out
        if(pthread_mutex_trylock(&lock) == 0)
        {
            printf("Unlocked");
            //signaler = SIG_WORKING;
            time(&start);
            //pthread_mutex_lock(&lock);
        }

        time(&now);
        if((now - start) >= 6)
        {
            break;
        }
    }
    free(buffer);
    pthread_exit(NULL);
}

int strtoport(char *str)
{
    int ret;
    ret = (int) strtol(str, (char **)NULL, 10);
    if (ret == 0)
    {
        printf("Invalid port number supplied: %s\n", str);
        return 0;
    }
    return ret;   
}