#include <dirent.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(void) {
// used to convert socket inode to progrma name
    
    DIR *d;
    struct dirent *dir;
    DIR *dtmp;
    struct dirent *tmp;
    char fdpath[100] = "/proc/";
    char temp[24] = "/proc/";
    char buf[1024];

    d = opendir("/proc");
    //Iterate through proc
    if (d) {
        while ((dir = readdir(d)) != NULL) {
        strcat(fdpath, dir->d_name);
        strcat(fdpath, "/fd");
        DIR *newd;
        struct dirent *fddir;
        newd = opendir(fdpath);
        //itterate through each fd looking for the 
        if(newd)
        {
            while((fddir = readdir(newd)) != NULL)
            {
                printf("%s\n", fddir->d_name);
                
                char socketfd[100] =  {0};
                memset(buf, 0, sizeof(buf));

                strcat(socketfd, fdpath);
                strcat(socketfd, "/");
                strcat(socketfd, fddir->d_name);

                //Resolving symlink to determine if this program has the open socket inode associated to it
                readlink(socketfd, buf, sizeof(buf)-1);
                printf("Link-> %s\n", buf);
                if(strstr(buf, "1291679") != NULL)
                {
                    printf("FOUND MATCH in %s\n", dir->d_name);
                    // Name of program is found in /proc/#/comm
                    char final[100] = "/proc/";
                    strcat(final, dir->d_name);
                    strcat(final, "/comm");
                    FILE *fp;
                    char buff[256];
                    fp = fopen(final, "r");
                    fscanf(fp, "%s", buff);
                    printf("%s\n", buff);

                    //cleanup
                    fclose(fp);
                    closedir(d);
                    closedir(newd);
                    exit(1);
                }
            }
        }
        strcpy(fdpath, temp);
        closedir(newd);
        }
        closedir(d);
    }
    return(0);
    }