#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>

jmp_buf buf1, buf2;
int buffer_size = 100;

void function1()
{
	printf("Function 1\n");

	char str[buffer_size];
	printf("Enter an integer value: ");
	scanf("%s", str);
	long value = strtol(str, NULL, 10);


	if (value)
	{
		printf("Valid input of: %d\n",value);
	}else{

		printf("Invalid input\n");
		longjmp(buf1, 1337);
	}
}



int main()
{
	int result;
	result = setjmp(buf1);
	if (result == 0) 
	{
		function1();
		printf("Exit Normally\n");
	}else{
		printf("Return Func1: %d\n", result);
	}
	return 0;
}

