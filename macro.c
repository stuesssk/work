#include <stdio.h> 


#define MYMACRO(num, str) ({\
            printf("%d", num);\
            printf(" is");\
            printf(" %s number", str);\
            printf("\n");\
           }) 


int main(void)
 {   
    int number;
    
    printf("Enter a number: "); 
    scanf("%d", &number);

    if (number & 1) 
        MYMACRO(number, "Odd"); 
    else
        MYMACRO(number, "Even"); 
  
    return 0; 
 }