#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>

#define MAX 65536

int main(int argc, char ** argv)
{
	if ( argc < 3 )
	{
		printf("Usage: %s <IP Address> <Min port> <Max port>");
	}
	

	int portStart, portEnd;
	char hostname[16];
	int port1, port2;

	port1 = strtol(argv[2], NULL, 10);
	port2 = strtol(argv[3], NULL, 10);

	if (port1 > port2)
	{
		portStart = port2;
		portEnd = port1;
	}else{
		portStart = port1;
		portEnd = port2;
	}
	strncpy(hostname, argv[1], 16);

	printf("IP: %s \nStarting Port: %i\nEndingPort: %i\n", hostname, portStart, portEnd);

	struct sockaddr_in sa;
    sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr(hostname);

	for (unsigned short port = portStart; port <= portEnd; ++port)//loop through and scan all ports
    {
		char host[128];
		char service[128];
        sa.sin_port = htons(port);
		//TCP scan
        int sock = socket(AF_INET, SOCK_STREAM, 0);

        if(socket < 0){
            exit(1);    
        }		
        int err = connect(sock, (struct sockaddr*)&sa, sizeof sa);
        if (err < 0){
            fflush(stdout);
        }
        else {
			getnameinfo((struct sockaddr *)&sa, sizeof(sa), host, sizeof(host), service, sizeof(service),0);
            printf("TCP Port %i is open, %s\n", port, service);
        }
        close(sock);

		//UDP Scan
		inet_pton(AF_INET, hostname, &sa.sin_addr.s_addr);
		sock = socket(AF_INET, SOCK_DGRAM, 0);

        if(socket < 0){
            exit(1);    
        }
        if(connect(sock, (struct sockaddr*)&sa, sizeof(struct sockaddr_in)) == 0)
		{
			struct timeval timeout;
			fd_set s;
			
			FD_ZERO(&s);
			FD_SET(sock, &s);

			timeout.tv_sec = 5;
			timeout.tv_usec = 0;

			send(sock, NULL, 0, 0);

			int result = select(sock + 1, &s, NULL, NULL, &timeout);
			switch(result){
			case 1:
			  break;

			case 0: // timeout
			  //TODO: Determine owning proram
			  getnameinfo((struct sockaddr *)&sa, sizeof(sa), host, sizeof(host), service, sizeof(service),NI_DGRAM);
			  printf("UDP Port %i is Open, %s\n", port, service);
			  break;
			case -1:
			   perror("select()"); 
			   exit (EXIT_FAILURE); 
			}
		 
			close(sock);
		}
	}
}
