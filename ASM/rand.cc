#include <benchmark/benchmark.h>

#include <fcntl.h>
#include <sys/random.h>
#include <sys/stat.h>
#include <unistd.h>

#include <fstream>
#include <iostream>




static void BM_Rand(benchmark::State& state) {
  // Perform setup here
  int num1;
  time_t t;
  //srand((unsigned) time(&t));
  for (auto _ : state) {
    // This code gets timed
    /*
      debating if seeding should be part of the setup or be part of the timed section
      since it only need to be done the first time

      If not part of the timed section rand() is actually faster than the ASM call
      -so the first call to rand() including the seeding function would be slower than
        an ASM call, but each successive call after would be significantly faster than an asm call
        -8.35 ns for regular rand() call 
        -124 ns for ASM call
        -1142 ns for rand() call + seeding
    */
    srand((unsigned) time(&t));
    num1 = rand();
  }
}
// Register the function as a benchmark
BENCHMARK(BM_Rand);


static void BM_ASM(benchmark::State& state) {
  // Perform setup here
  char rc;   
  size_t val; 
  for (auto _ : state) {
    // This code gets timed
    __asm__ volatile(
        "rdrand %0 ; setc %1"
        : "=r" (val), "=qm" (rc)
        :
        :"edx");

    if (rc == 0)
    {
        exit(1);
    }
  }
}
BENCHMARK(BM_ASM);


static void BM_GetRandom(benchmark::State& state) {
  // Perform setup here
  size_t bufsz = 50;
  char *buf;
  ssize_t ret; 
  for (auto _ : state) {
    // This code gets timed
    buf = (char*)malloc(bufsz);
    ret = getrandom(buf, bufsz, GRND_RANDOM);
    if (ret == -1) 
    {
        exit (1);
    }
  }
}
BENCHMARK(BM_GetRandom);


static void BM_URandom(benchmark::State& state) {
  // Perform setup here
  size_t bufsz = 50;
  char *buf;
  ssize_t ret; 
  for (auto _ : state) {
    // This code gets timed
    unsigned long long int random_value = 50; //Declare value to store data into
    size_t size = sizeof(random_value); //Declare size of data
    std::ifstream urandom("/dev/urandom", std::ios::in | std::ios::binary); //Open stream
    if(urandom) //Check if stream is open
    {
        urandom.read(reinterpret_cast<char*>(&random_value), size); //Read from urandom
        if(!urandom) //Check if stream is ok, read succeeded
        {
            std::cerr << "Failed to read from /dev/urandom" << std::endl;
        }
        urandom.close(); //close stream
    }
    else //Open failed
    {
        std::cerr << "Failed to open /dev/urandom" << std::endl;
    }
  }
}
BENCHMARK(BM_URandom);

// Run the benchmark
BENCHMARK_MAIN();