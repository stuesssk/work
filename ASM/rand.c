#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/random.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

// Attempted to use google benchmark. didn't realize it was C++.
// Will circle back to google benchmark after brushing up and becoming
// more familiar with C++. Spent a while trying to trouble shoot
// before realizing it was for C++



int main (void)
{
    int num1, num2, num3, num4;
    double elapsed = 0.0;
    // rand()
    // returns a pseudo-random number in the range of 0 to RAND_MAX. guranteed to be at least 32767
    //~2.5 times slower than the asm call
    clock_t begin = clock();
    num1 = rand();
    clock_t end = clock();

    elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("rand() elapsed time: %f\n", elapsed);


    // RDRAND
    //Fastest
    char rc;   
    size_t val; 
    begin = clock();
    __asm__ volatile(
        "rdrand %0 ; setc %1"
        : "=r" (val), "=qm" (rc)
        :
        :"edx");

    if (rc == 0)
    {
        exit(1);
    }

    end = clock();
    elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("RDRAND elapsed time: %f\n", elapsed);

    /*
        getrandom and reading from dev/urandom scale with the size of the buffer
        you are writting to, but even with a small 50 char bufer. They are still
        2-3x slower than thr rand() call and an order of magnitude slower than RDRAND
    */

    // getrandom()
    size_t bufsz = 50;
    char *buf;
    ssize_t ret;
 
    begin = clock();
    buf = (char*)malloc(bufsz);
    ret = getrandom(buf, bufsz, GRND_RANDOM);
    if (ret == -1) 
    {
        exit (1);
    }
    end = clock();
    elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("getrandom() elapsed time: %f\n", elapsed);

    // dev/urandom

    begin = clock();
    int randomData = open("/dev/urandom", O_RDONLY);
    if (randomData < 0)
    {
        // something went wrong
        exit (2);
    }
    char myRandomData[50];
    ssize_t result = read(randomData, myRandomData, sizeof myRandomData);
    if (result < 0)
    {
        // something went wrong
        exit(3);
    }
    end = clock();
    elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("dev/urandom elapsed time: %f\n", elapsed);




}