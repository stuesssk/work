#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
void __unlock(pthread_mutex_t ** lock);




pthread_t tid[2];
int counter;
// can't use cleanup on global variable
pthread_mutex_t lock;


/* 
Can't use a multiline macro since the mutex will fall out
of scope once the macro code is done executing, when using a 
multi line macro. took me a good amount of time to realize that
*/
#define MYMACRO(thelock) printf("locking\n");pthread_mutex_t *local_lock __attribute__ ((__cleanup__(__unlock))) = &thelock;pthread_mutex_lock(local_lock);


void __unlock(pthread_mutex_t ** mylock)
{
    printf("Unlocking\n");
    pthread_mutex_unlock(*mylock);
}

void* doSomeThing(void *arg)
{
    //using local mutex to control global mutex 
    pthread_mutex_t *local_lock __attribute__ ((__cleanup__(__unlock))) = &lock;
    // Because of cleanup, mutex will unlock global mutex when the local one falls out of scope
    pthread_mutex_lock(local_lock);
    //MYMACRO();

    unsigned long i = 0;
    counter += 1;
    printf("\n Job %d started\n", counter);

    for(i=0; i<(0xFFFFFFFF);i++);

    printf("\n Job %d finished\n", counter);

    return NULL;
}

int main(void)
{
    int i = 0;
    int err;

    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("\n mutex init failed\n");
        return 1;
    }

    while(i < 2)
    {
        err = pthread_create(&(tid[i]), NULL, &doSomeThing, NULL);
        if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));
        i++;
    }

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_mutex_destroy(&lock);

    return 0;
}