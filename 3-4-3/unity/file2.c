#include "file2.h"

int func1(int args)
{
    printf("in %s\n", __func__);
    int ret = 0;

    FILE *fp = fopen("unity_test.txt", "a");
    if (NULL == fp) 
    {
	ret = 1;
    }
    else
    {
        for (int i = 0; i <= args; ++i)
    	{
    	    fputc(i + 97, fp);
    	    fputc('\n', fp);
    	}
	    fclose(fp);

        ret = func2(args);

    }
    return ret;
}