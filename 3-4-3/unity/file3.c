int func2(int num_args)
{
    printf("in %s\n", __func__);
    int ret = 0;

    FILE *fp = fopen("unity_test.txt", "a");
    if (NULL == fp) 
    {
	ret = 1;
    }
    else
    {
        for (int i = 0; i <= num_args; ++i)
    	{
    	    fputc(i + 65, fp);
    	    fputc('\n', fp);
    	}
	    fclose(fp);
    }
    return ret;
}