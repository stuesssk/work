#include <stdio.h>

#include "file1.h"


int main(int argc, char *argv[])
{
    printf("Starting function: %s\n", argv[0]);

    int ret = 0;

    ret = func1(argc);

    return ret;
}