#include <stdio.h>

#define sstr(s) str(s)
#define str(s) #s
#define NUMBER 12

#define STR_CONCAT(x,y) x##y

#define STRINGIZER(x) printf (#x "\n")


int main(int argc, char *agrv[])
{
    printf("Number: %s\n", sstr(NUMBER));
    printf("Number: %s\n", str(NUMBER));


    // Doesn't expand variables
    // Since macros get expanded in the preprocessor
    int i = 42;
    printf("Number: %s\n", sstr(i));


    int ij = 42;
    printf("Number: %d\n", STR_CONCAT(i, j));

    STRINGIZER(text);




    printf("Number: %d\n", STR_CONCAT(12, 50));
}