#include <stdio.h>
#include <string.h>

/*
 ## needed for when the __VA_ARGS__ are not used
  -C does not allow the variable argument to be left out without it
*/
// Throws a warning when only giving one argument
// ISO C99 requires at least one argument for the "..." in a variadic macro
#define ERR_PRINT1(format, ...) fprintf(stderr, format, ##__VA_ARGS__)

// Can also specify with no named arguments and all variable args
// **proper way to not trow any warnings when only giving one argument to the macro**
#define ERR_PRINT2(...) fprintf(stderr, __VA_ARGS__)

// Able to rename the __VA_ARGS__ to some more descriptive if necessary in more complex macro
// -though when compiled -Wpedantic will throw a warning that ISO C does not permit named variadic macros
#define ERR_PRINT3(args...) fprintf(stderr, args)

/*
  Another way to handle when variable args may not be given
    if variable argument has any tokens, then a __VA_OPT__ invocation expands to its argument; 
    but if the variable argument does not have any tokens, the __VA_OPT__ expands to nothing
*/
// Throws a warning when only giving one argument
// ISO C99 requires at least one argument for the "..." in a variadic macro
#define ERR_PRINT4(format, ...) fprintf(stderr, format __VA_OPT__(,) __VA_ARGS__)



int main(void)
{
    printf("start\n");
    ERR_PRINT1("Success\n");
    ERR_PRINT2("Error %d %d %d\n", 1,2,3);
    ERR_PRINT2("Success Aain\n");
    ERR_PRINT3("Error %d %d\n", 2,3);
    ERR_PRINT4("Another Success!\n");


}