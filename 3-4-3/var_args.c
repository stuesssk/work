#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>


void my_sum(int values, ...);
void myvargs();
void my_printf(char* format, ...);



int main()
{
    my_sum(4,1,2,3,4);
    myvargs(6,1,2,3,4,5,6);
    my_printf("Some values: %d, %s, %c!", 4, "foo", 'z');
}

void my_sum(int values, ...)
{
    va_list argp;
    // Requires the last fixed parameter (to get the address)
    va_start(argp, values);
    int total = 0;


    for(int i = 0; i<values; ++i)
    {
        //  Increments argp to the next argument
        total += va_arg(argp, int);
    }
    // clean up
    // If needed to scan list a 2nd time would need to first end then start again
    va_end(argp);
    printf("total: %d\n", total);
}

void myvargs()
{
    /*
         built-in functions may interact badly with some sophisticated features or 
         other extensions of the language. 
         It is, therefore, not recommended to use them outside very simple functions 
         acting as mere forwarders for their arguments --gcc gnu help page
    */
    void *arg = __builtin_apply_args();
    void *ret = __builtin_apply((void*)my_sum, arg, 64);
    __builtin_return(ret);

}

void my_printf(char* format, ...)
{
    // Iterate through the format string to know how many var args 
    // there should be
    va_list argp;
    va_start(argp, format);
    while (*format != '\0') {
        if (*format == '%') {
            format++;
            if (*format == '%') {
            putchar('%');
            } else if (*format == 'c') {
            char char_to_print = va_arg(argp, int);
            putchar(char_to_print);
            } else if (*format == 'd') {
            int integer = va_arg(argp, int);
            printf("%d",integer);
            } else {
            fputs("Not implemented", stdout);
            va_arg(argp, int);
            }
        } else {
            putchar(*format);
        }
    format++;
    }
    va_end(argp);
    puts("");
}
