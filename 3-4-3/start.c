/*
Simple file to get used to rewritting _start function
-needs to be compiled with the -nostartfiles option as the original _start
    is being over writen
*/
#include <stdio.h>
#include <stdlib.h>

void nextfunc(void);
void otherfunc(void);

/* 
exit() and _exit() 
the former performs clean-up related to user-mode constructs in the library, 
    and calls user-supplied cleanup functions, 
whereas the latter performs only the kernel cleanup for the process
*/
extern void _exit(register int);

void _start(void)
{
    printf("Inside _start\n");
    nextfunc();
    _exit(0);
}

void nextfunc(void)
{
    printf("Inside next function\n");
    otherfunc();
}

void otherfunc(void)
{
    printf("Inside Other\n");
}
