#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>

// Implementing simple four function calculator

jmp_buf buf1, buf2;
int buffer_size = 100;


void inputSymbol(int * symbol)
{
	int choice = 0;
	printf("Pick a function:\n 1)/\n2)*\n3)-\n4)+\n");
    scanf(" %d", &choice);
    switch (choice)
    {
    case 1:
    case 2:
    case 3:
    case 4:
        *symbol = choice;
        return;
    default:
        printf("invalid selection\n");
        return;
    }
}


void inputValue(long int * userInput)
{
	long i;
	printf("Enter an integer value: ");
	if (scanf("%ld", &i) != 1)
	{
		longjmp(buf1, 1337);
	}
    *userInput = i;
    
}

void testIntegers(long int * number)
{
    int result;
    result = setjmp(buf1);
    if(result == 0)
    {
        inputValue(number);
    }else{
        printf("Invalid Int\n");
        exit(1);
    }
}

void testSymbols(int *symbol)
{
    int result;
    result = setjmp(buf2);
    if(result == 0)
    {
        inputSymbol(symbol);
    }else{
        printf("Invalid symbol");
        exit(1);
    }       
}

void doMath(int mathSymbol, long int *num1, long int *num2)
{
    int result;
    switch (mathSymbol)
    {
    case 1:
        // only deal with integer division for now
        result =  *num1 / *num2;
        break;
    case 2:
        result = *num1 * *num2;
        break;
    case 3:
        result = *num1 - *num2;
        break;
    case 4:
        result = *num1 + *num2;
        break;
    
    default:
        break;
    }
    printf("Result: %d\n", result);

}

int main()
{
    long int num1, num2;
    int mathSymbol;
	
    //printf("Symbol: %d\n", mathSymbol);
    testIntegers(&num1);
    printf("integer: %ld\n", num1);
    testSymbols(&mathSymbol);
    printf("Symbol: %d\n", mathSymbol);
    testIntegers(&num2);
    printf("integer: %ld\n", num2);
    //mathSymbol = 1;
    doMath(mathSymbol, &num1, &num2);
}
