#define _GNU_SOURCE

#include <dirent.h> 
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

void readfile(const char * filename, int type);
void strSplit(char * string, int type);
void splitIpPort(char * string);
void convertinode(char * name, char *inode);

int main(void)
{
    const char tcpFile[] = "/proc/net/tcp";
    const char udpFile[] = "/proc/net/udp";
    printf("Protocol  Local Address          Foreign Address      Pid/Program Name\n");
    readfile(tcpFile, 1);
    readfile(udpFile, 2);
}

/*
* reading the files line by line
*/
void readfile(const char * filename, int type)
{
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    bool first = true;

    fp = fopen(filename, "r");
    if (fp == NULL)
    {

        printf("EXITING\n");
        exit(EXIT_FAILURE);
    }
    while ((read = getline(&line, &len, fp)) != -1) {
        //printf("Retrieved line of length %zu:\n", read);
        if(first)
        {
            // Don't need the column names
            first = false;
            continue;
        }
        strSplit(line, type);
        //printf("%s", line);
    }

    fclose(fp);
    if (line)
        free(line);

}


/*
* Splitting each line from the file to extract the necessary data
*/
void strSplit(char * string, int type)
{
    size_t count = 1;
    char *progName = calloc(128, 1);
	char delim[] = " ";

	char *ptr = strtok(string, delim);
	while(ptr != NULL)
	{
        
        switch (count)
        {
        case 2:
            // Print weather it's a tcp or udp port
            if(type == 1)
            {
            printf("tcp       ");
            }else{
            printf("udp       ");
            }
        case 3:
            // Split ip and port and print
            splitIpPort(ptr);
            break;
        case 10:
            // socket inode number needs to be converted to service name
            convertinode(progName, ptr);
            printf("%s\n", progName);
            // need to ensure old program name is zeroed out before next converison
            memset(&progName, 0, sizeof(progName));
            break;
        }
        if(count == 10){
            // don't care about the rest of the columns right now
            break;
        }
		ptr = strtok(NULL, delim);
        ++count;
	}
}
/*
* extracting ip and port from the colon sperated string 
*/
void splitIpPort(char * string)
{
    //TODO convert port and IP to human readable
    char ipStr[12];
    char portStr[8];
    memset(ipStr, '\0', sizeof(ipStr));
    memset(portStr, '\0', sizeof(portStr));

    strncpy(ipStr, string, 8);
    size_t ip = (int)strtol(ipStr, NULL, 16);
    struct in_addr addr;
    addr.s_addr = ip; // s_addr must be in network byte order 
    char *s = inet_ntoa(addr);
    // Print IP address
    printf("%s:", s);

    strcpy(portStr, &string[9]);
    size_t port = (int)strtol(portStr, NULL, 16);
    //Print port number
    printf("%-11zd", port);
}


/*
* /proc/net/tcp and /proc/net/udp list inode number for the sockets
* need to convert inode to pid, then pid to program name
* need to iterate over each /proc/<PID> directories
* then itereate over each file descriptor in /proc/<PID>/fd
* then resolve the symlink for the file descriptors
* Once a matching file descriptor is found with /proc/net/tcp or udp
*   program name is found in /proc/<PID>/comm
*/
void convertinode(char * name, char *inode)
{
    DIR *d;
    struct dirent *dir;
    DIR *dtmp;
    struct dirent *tmp;
    char fdpath[100] = "/proc/";
    char temp[8] = "/proc/";
    char buf[1024];

    d = opendir("/proc");
    //Iterate through proc
    if (d) {
        while ((dir = readdir(d)) != NULL) {
        strcat(fdpath, dir->d_name);
        strcat(fdpath, "/fd");
        DIR *newd;
        struct dirent *fddir;
        newd = opendir(fdpath);
        //iterate through each fd in /proc/#/fd
        if(newd)
        {
            while((fddir = readdir(newd)) != NULL)
            {
                char socketfd[100] =  {0};
                memset(buf, 0, sizeof(buf));

                //Building string to search /proc/#/fd for all file descriptors
                strcat(socketfd, fdpath);
                strcat(socketfd, "/");
                strcat(socketfd, fddir->d_name);

                //Resolving symlink to determine if this program has the open socket inode associated to it
                readlink(socketfd, buf, sizeof(buf)-1);

                if(strstr(buf, inode) != NULL)
                {
                    // Name of program is found in /proc/#/comm
                    char final[100] = "/proc/";
                    strcat(final, dir->d_name);
                    strcat(final, "/comm");
                    FILE *fp;
                    char buff[256];
                    fp = fopen(final, "r");
                    fscanf(fp, "%s", name);

                    //cleanup
                    fclose(fp);
                    closedir(d);
                    closedir(newd);
                    return;
                }
            }
        }
        strcpy(fdpath, temp);
        closedir(newd);
        }
        closedir(d);
    }
}